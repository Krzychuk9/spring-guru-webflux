package pl.kasprowski.springgurureactivewebfluxservice.service;

import org.junit.Test;
import pl.kasprowski.springgurureactivewebfluxservice.domain.Quote;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.concurrent.CountDownLatch;
import java.util.function.Consumer;

public class QuoteGeneratorServiceImplTest {

    @Test
    public void fetchQuoteStream() throws Exception {
        CountDownLatch countDownLatch = new CountDownLatch(1);
        Consumer<Quote> onNext = System.out::println;
        Consumer<Throwable> onError = e -> System.out.print(e.getMessage());
        Runnable onComplete = countDownLatch::countDown;

        QuoteGeneratorService sut = new QuoteGeneratorServiceImpl();
        Flux<Quote> quoteFlux = sut.fetchQuoteStream(Duration.ofMillis(1000L));

        quoteFlux.take(10).subscribe(onNext, onError, onComplete);

        countDownLatch.await();
    }
}