package pl.kasprowski.springgurureactivewebfluxservice.web;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.FluxExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;
import pl.kasprowski.springgurureactivewebfluxservice.domain.Quote;
import reactor.test.StepVerifier;

import java.util.concurrent.CountDownLatch;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class QuoteRouterTest {

    private WebTestClient webTestClient;

    @Autowired
    private RouterFunction<ServerResponse> quoteRoute;

    @Before
    public void setUp() {
        webTestClient = WebTestClient
                .bindToRouterFunction(quoteRoute)
                .build();
    }

    @Test
    public void testFetchQuotes() {
        webTestClient
                .get()
                .uri("/quotes?size=20")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBodyList(Quote.class).hasSize(20)
                .consumeWith(quotes -> {
                    assertThat(quotes.getResponseBody()).allSatisfy(quote -> assertThat(quote.getPrice()).isPositive());
                    assertThat(quotes.getResponseBody()).hasSize(20);
                });
    }

    @Test
    public void testStreamQuotes() throws Exception {
        CountDownLatch countDownLatch = new CountDownLatch(10);

        webTestClient
                .get()
                .uri("/quotes")
                .accept(MediaType.APPLICATION_STREAM_JSON)
                .exchange()
                .returnResult(Quote.class)
                .getResponseBody()
                .take(10)
                .subscribe(quote -> {
                    assertThat(quote.getPrice()).isPositive();
                    countDownLatch.countDown();
                });

        countDownLatch.await();
    }

    @Test
    public void testStreamQuotesWithStepVerify() {
        FluxExchangeResult<Quote> result = webTestClient
                .get()
                .uri("/quotes")
                .accept(MediaType.APPLICATION_STREAM_JSON)
                .exchange()
                .returnResult(Quote.class);

        StepVerifier.create(result.getResponseBody())
                .expectNextCount(3)
                .consumeNextWith(quote -> assertThat(quote.getTicker()).isEqualTo("ORCL"))
                .thenCancel()
                .verify();

    }
}