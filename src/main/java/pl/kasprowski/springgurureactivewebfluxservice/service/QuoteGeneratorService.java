package pl.kasprowski.springgurureactivewebfluxservice.service;

import pl.kasprowski.springgurureactivewebfluxservice.domain.Quote;
import reactor.core.publisher.Flux;

import java.time.Duration;

public interface QuoteGeneratorService {
    Flux<Quote> fetchQuoteStream(Duration period);
}
