package pl.kasprowski.springgurureactivewebfluxservice.web;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import pl.kasprowski.springgurureactivewebfluxservice.domain.Quote;
import pl.kasprowski.springgurureactivewebfluxservice.service.QuoteGeneratorService;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;

import static org.springframework.web.reactive.function.server.ServerResponse.ok;

@Component
public class QuoteHandler {

    private static final Duration DURATION = Duration.ofMillis(100L);

    private final QuoteGeneratorService service;

    public QuoteHandler(final QuoteGeneratorService service) {
        this.service = service;
    }

    Mono<ServerResponse> fetchQuotes(ServerRequest request) {
        int size = Integer.parseInt(request.queryParam("size").orElse("10"));

        Flux<Quote> quoteFlux = service
                .fetchQuoteStream(DURATION)
                .take(size);

        return ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(quoteFlux, Quote.class);
    }

    Mono<ServerResponse> streamQuotes(ServerRequest request) {
        Flux<Quote> quoteFlux = service.fetchQuoteStream(DURATION);

        return ok()
                .contentType(MediaType.APPLICATION_STREAM_JSON)
                .body(quoteFlux, Quote.class);
    }
}
