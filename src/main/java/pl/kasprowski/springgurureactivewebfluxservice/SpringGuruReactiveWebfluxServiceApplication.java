package pl.kasprowski.springgurureactivewebfluxservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringGuruReactiveWebfluxServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringGuruReactiveWebfluxServiceApplication.class, args);
    }
}
